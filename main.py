from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time, fire
from getpass import getpass
import keyring
from package_checker import PackChecker

class ScheludeVGUES:
    PackChecker().check('./requirements.txt') # загрузка необходимых зависимостей, если их нет

    service_id = "service_id"

    def __init__(self):
        self.__user_login = None
        self.__user_password = None

    def add_user_data(self):
        print('\nЧтобы данные для входа не требовалось запрашивать снова, они будут сохранены\n')
        self.__user_login = input("Введите ваш логин студента ВГУЭС'а: ")
        self.__user_password = getpass("Введите ваш пароль: ")
        keyring.set_password(self.service_id, self.__user_login, self.__user_password)

    def get(self, category):
        user_data = keyring.get_credential(self.service_id, None)
        if user_data is not None:
            self.__user_login = user_data.username
            self.__user_password = user_data.password
        else:
            self.add_user_data()

        # Параметры, касающиеся выполнения веб-браузера
        options = webdriver.ChromeOptions() # Инициализация опций Chrome
        options.add_argument("--headless")
        options.add_argument("--disable-extensions")
        options.add_argument("--window-size=%s" % "800, 600")
        browser = webdriver.Chrome(options=options) # Запуск драйвера браузера

        browser.get("https://cabinet.vvsu.ru/sign-in") # Запустить браузер по этому адресу
        browser.find_element(By.NAME, "login").send_keys(self.__user_login)
        time.sleep(0.1)
        browser.find_element(By.NAME, "password").send_keys(self.__user_password)
        time.sleep(0.1)
        browser.find_element(By.NAME, "submit").send_keys(Keys.ENTER)
        time.sleep(1)
        browser.get("https://cabinet.vvsu.ru/time-table/")
        if category == "this":
            try:
                # Определение атрибута веб-страницы по имени класса и содержимого атрибута
                sc = browser.find_element(By.CLASS_NAME, "jcarousel-container").text
                print(f"\n{sc}\n")
            except Exception as e:
                print(e)
            finally:
                browser.close()
                browser.quit()
        elif category == "next":
            try:
                browser.find_element(By.CLASS_NAME, "jcarousel-next").click()
                time.sleep(1)
                container = browser.find_element(By.XPATH, '//*[@id="mycarousel"]')
                content = container.find_element(By.XPATH, '//*[@id="mycarousel"]/li[2]').text
                print(f"\n{content}\n")
            except Exception as e:
                print(e)
            finally:
                browser.close()
                browser.quit()
        else:
            print("\n\nERROR: В аргументе должно быть либо this, либо next.\n\n")

# Для работы модуля fire, чтобы запустить программу через консоль
if __name__ == "__main__":
    fire.Fire(ScheludeVGUES)

# Отобразить расписание на эту неделю
# ScheludeVGUES().get("this")

# Отобразить расписание на следующую неделю
# ScheludeVGUES().get("next")

